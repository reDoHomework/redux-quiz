# home page

### show notes list
* show all notes
* show note titles list
* click note titles to link to note details page
* notes list style
### create note
### note header and icons

# note detail page

* show title and content
* show left aside note title list
* left aside background color and no scroll

# create note

* create new note and show in home page
* title and content can not be null
* when disable button highlight 20%
* cancel back to home page

# delete note

* delete note and return to home page
* hover style

# others

* show note in md style
* test

