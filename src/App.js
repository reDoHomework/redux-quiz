import React, {Component} from 'react';
import './App.less';
import Home from "./page/home";
import NoteDetails from "./page/noteDetails";
import {BrowserRouter as Router,Route, Link, Switch} from "react-router-dom";
import Header from "./component/header";
import NoteCreate from "./page/noteCreate";


class App extends Component {
    render() {
        return (
            <div className='App'>
                <Header/>
                <Router>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/note/:id" component={NoteDetails}/>
                        <Route path="/notes/create" component={NoteCreate}/>
                    </Switch>
                </Router>
            </div>
        );
    }
}

export default App;

