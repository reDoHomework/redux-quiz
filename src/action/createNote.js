export const setTitle = (title) => {
    return {
        type: "SET_TITLE",
        title: title
    }
};

export const setContent = (content) => {
    return {
        type: "SET_CONTENT",
        content: content
    }
};