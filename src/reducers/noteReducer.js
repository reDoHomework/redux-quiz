const initial = {
    notes: [],
    title: null,
    content: null
};

export const noteReducer = (state = initial, action) => {
    switch (action.type) {
        case "GET_ALL_NOTES":
            return {
                ...state,
                notes: action.payload
            };
        case "SET_TITLE":
            return {
                ...state,
                title: action.title
            };
        case "SET_CONTENT":
            return {
                ...state,
                content: action.content
            };
        default:
            return state;
    }
};