import React, {Component} from 'react';
import { MdEventNote } from "react-icons/md";

class Header extends Component {
    render() {
        return (
            <div className="header">
                <MdEventNote size={'40'}/>
                <p>NOTE</p>
            </div>
        );
    }
}

export default Header;