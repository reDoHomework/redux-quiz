import React, {Component} from 'react';
import {getAllNotes} from "../action/getAllNotes";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {MdLibraryAdd} from "react-icons/md";

class Home extends Component {


    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        this.props.getAllNotes();
    }

    render() {
        return (
            <div className="content">
                {this.props.notes.map(note =>
                    <div className="homeNoteTitles">
                        <Link to={{
                            pathname: `/note/${note.id}`,
                            title: note.title,
                            description: note.description
                        }}>{note.title}</Link>
                    </div>)}
                <div className="homeNoteTitles">
                    <Link to="/notes/create">
                        <MdLibraryAdd size={40}/>
                    </Link>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    notes: state.noteReducer.notes
});

const mapDispatchToProps = (dispatch) => ({
    getAllNotes: () => {
        dispatch(getAllNotes())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);