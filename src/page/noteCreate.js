import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {setContent, setTitle} from "../action/createNote";
import connect from "react-redux/es/connect/connect";

class NoteCreate extends Component {

    constructor(props, context) {
        super(props, context);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleContentChange = this.handleContentChange.bind(this);
        this.submitNote = this.submitNote.bind(this);
        this.cancelCreateNote = this.cancelCreateNote.bind(this);
    }

    render() {
        return (
            <div className="createNote">
                <h1>创建笔记</h1>
                <hr/>
                <label>
                    标题<br/>
                    <input type="text" onChange={this.handleTitleChange}/>
                </label>
                <br/>
                <label>
                    正文<br/>
                    <textarea type="text" onChange={this.handleContentChange}/>
                </label>
                <br/>
                <button onClick={this.submitNote}
                        disabled={this.props.title == null || this.props.content == null}
                        className="submit">
                    提交
                </button>
                <button onClick={this.cancelCreateNote}>取消</button>
            </div>
        );
    }

    handleTitleChange(event) {
        this.props.setTitle(event.target.value);
    }

    handleContentChange(event) {
        this.props.setContent(event.target.value);
    }

    submitNote() {
        console.log(this.props.title);
        const note = {title: this.props.title, description: this.props.content};

        fetch("http://localhost:8080/api/posts",
            {method: "POST", body: JSON.stringify(note), headers: {'Content-Type': 'application/json'}})
            .then(
                this.props.history.push("/")
            );
    }

    cancelCreateNote() {
        this.props.history.push("/");
    }
}

const mapStateToProps = state => ({
    title: state.noteReducer.title,
    content: state.noteReducer.content
});

const mapDispatchToProps = dispatch => bindActionCreators({
    setTitle, setContent
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NoteCreate);