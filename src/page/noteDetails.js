import React, {Component} from 'react';

class NoteDetails extends Component {

    constructor(props, context) {
        super(props, context);
        this.deleteNote = this.deleteNote.bind(this);
    }

    render() {
        return (
            <div>
                {this.props.location.title}<br/>
                {this.props.location.description}
                <br/>
                <button onClick={this.deleteNote} className="delete">
                    删除
                </button>
                <button className="return">返回</button>
            </div>
        );
    }

    deleteNote() {
        const fetchURI = "http://localhost:8080/api/posts/" + this.props.match.params.id;
        fetch(fetchURI, {
            method: 'DELETE'
        }).then(
            this.props.history.push("/")
        );
    }
}

export default NoteDetails;